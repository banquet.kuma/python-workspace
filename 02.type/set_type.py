#set : 중복허용하지 않고 add한 순서 상관없이 저장
s=set("Hello")
s1=set([1,2,3])

print(s, s1)

#집합 함수
s2=set([1,2,3,4,5,6])
s3=set([4,5,6,7,8,9])

print("교집합 : {0}".format(s2.intersection(s3)) )
print("합집합 : {0}".format(s2.union(s3)) )
print("차집합 : {0}".format(s2.difference(s3)) )

#set 함수
s2.add(7)
print("add {0}".format(s2))

s2.update([8,9,10])
print("update {0}".format(s2))

s2.remove(10)
print("remove {0}".format(s2))

#type() : 변수의 type 확인
print("s2 type {0}".format(type(s2)))
print(type(True))