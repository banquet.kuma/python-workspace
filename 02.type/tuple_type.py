#tuple : (data_list)  값 변경하지 않고 read only
t = (1,2,3)
print(t, t[0], t[1], t[2])

#tuple 요소 삭제 
# del t[0] #TypeError: 'tuple' object doesn't support item deletion

#tuple 요소 변경
# t[0]=3 #TypeError: 'tuple' object does not support item assignment
