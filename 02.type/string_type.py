print('hello')
print("hello")
print("\"Life is too short\n You need python!!")

head="Pyhon"
tail=" 3.9"

pythonversion = head + tail
print(pythonversion)
print( len(pythonversion) )

print(pythonversion[0])
print(pythonversion[8])
# print(pythonversion[9])  #IndexError : 0~length-1
print(pythonversion[-1])

slicing = pythonversion[0:5] # 0~4
print(slicing)
print(pythonversion[:5]) #0~4  Python
print(pythonversion[5:]) #5~length-1  3.9
print(pythonversion[:])  #all Python 3.9

# string format
print("I eat %d apples" %3)
number = 3
print("I eat %d apples" % number)
# print("I eat %d apples" % '3')  # Type Error
print("I eat %s apples" % "three")
print("I eat %s apples" % number) #number - string 변환
day="three"
print("I ate %d apples. so I was sick for %s days." % (number, day))

print( "%10s" % "hi")
print("%10.4f" % 3.42134234)

print( "I ate {0} apples. so I was sick for {1} days.".format(number, day))
print( "I ate {number} apples. so I was sick for {day} days.".format(number=3, day="three"))
print(f"I ate {number} apples. so I was sick for {day} days.") #python 3.6이상

#string function
#find - string index return
print("pythonversion 2 ? ", pythonversion.find('2'))  # return -1 - pythonversion 2문자열 미포함
print("pythonversion 3 ? ", pythonversion.find('3'))  # return 6  - index 6 3문자열 포함

print(pythonversion, pythonversion.upper()) #대문자 변환
print(pythonversion, pythonversion.lower()) #소문자 변환
print("   space    ".strip())  #공백 삭제