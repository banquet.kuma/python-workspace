class Calculator :
    static_variable = "계산기" #class variable : 모든 객체가 공유
    def __init__(self):   #생성자
        self.result = 0   #member variable (클래스의 속성) 초기화
        print("Calculator 생성자 호출")
    
    def add(self, num) :  #member method (클래스 기능)
        self.result += num
        return self.result



#use code - classtype의 object 
cal1 = Calculator()  #생성자이용해서 instance화 - cal1 객체(object) 초기화 self.result=0
cal2 = Calculator()  #생성자이용해서 instance화 - cal2 객체(object) 초기화 self.result=0

print( cal1.add(3) )  #cal1객체의 add method 호출   3
print( cal1.add(4) )  #                            7

print( cal2.add(3) )  #cal2객체의 add method 호출   3
print( cal2.add(7) )  #                            

#클래스 상속 : 부모클래스(Super Class)의 member variable과 member method를  자식클래스(Sub Class)에서 사용
# class SubClass(SuperClass) : 
class SubClass(Calculator):
    def __init__(self):
        super().__init__()
        print("SubClass의 생성자 호출")
    
    def add(self, num) :  #method overriding : 부모의 method를 재정의 - 재정의 한 override method가 응답
        return num+10

sub_variable = SubClass()  # Super Class의 생성자 호출 --> Sub Class의 생성자 호출
print( "SubClass에서 SuperClass method사용하기 {0} ".format(sub_variable.add(3)))



#Car 추상화를 통한 attribute(member variable), operator(member method)
class Car :
    def __init__(self, name, cc, color):
        self.name =name
        self.cc = cc
        self.color=color
   
    def car_info(self) :
        print("{0}차는 {1}의 배기량 {2}cc 입니다. ".format(self.name, self.color, self.cc))



#객체생성 및 초기화 - 생성자
jsh = Car("롤스로이스", 3000, "흰색")
hsl = Car("그랜저", 3000, "검정")

#객체 기능 사용 - dot(.)
jsh.car_info()
hsl.car_info()