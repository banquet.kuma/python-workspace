# 초기식
# while 조건식 : 
#     참일때 실행문
#     증감식
# 무한루프
#  while True :
#      무한루프 실행문
#      if 루프 종료 조건식 :
#         break;

number=1
while number < 11 :
    print(number)
    number += 1


#무한루프 - break : 명령문 벗어나기
while True:
    money = int(input("돈을 넣어주세요 : "))
    if money < 0 :
        break;
    print("커피가 나왔습니다.")    



#for 변수 in list(tuple, string) :
#    참일때 루프 실행문
#for 변수 in range(minumber, maxnumber ) : minmuber~ maxmuber-1
#    참일때 루프 실행문

num = [1,2,3,4,5,6,7,8,9,10]
for i in num :
    print(i)

for i in range(0,10) :  
    print(i+1)


#continue : 아래실행문 실행하지 않고 가까운 루프문으로 돌아가기
#1~10 홀수만 출력
for i in range(1, 11) :
    if i%2 == 0 :
        continue
    print("홀수 {0}".format(i))


#nested loop
for i in range(1,10):        # ①번 for문
    for j in range(2, 10):   # ②번 for문
        print(i*j, end="\t") 
    print('') 