#파일 읽기 - 파일 쓰기

f = open("test.txt", 'r')   #with open("test.txt", 'r') as f : 
                            #->with문으로 파일 사용시  f.close() 자동
fw = open("test_copy.txt", 'w')

data = f.read()
fw.write(data)

f.close()
fw.close()

